"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const address_controller_1 = require("../controllers/address.controller");
const router = (0, express_1.Router)();
router.get('/direcciones', address_controller_1.getAddresses);
router.get('/direcciones/:id', address_controller_1.getAddress);
router.get('/direcciones/clientes/:id', address_controller_1.getAddressForClient);
router.post('/direcciones', address_controller_1.postAddress);
router.put('/direcciones/:id', address_controller_1.putAddress);
router.delete('/direcciones/:id', address_controller_1.deleteAddress);
exports.default = router;
//# sourceMappingURL=address.route.js.map