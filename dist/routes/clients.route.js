"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const clients_controller_1 = require("../controllers/clients.controller");
const router = (0, express_1.Router)();
router.get('/clientes', clients_controller_1.getClients);
router.get('/clientes/:id', clients_controller_1.getClient);
router.post('/clientes', clients_controller_1.postClient);
router.put('/clientes/:id', clients_controller_1.putClient);
router.delete('/clientes/:id', clients_controller_1.deleteClient);
exports.default = router;
//# sourceMappingURL=clients.route.js.map