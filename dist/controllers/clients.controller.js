"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteClient = exports.putClient = exports.postClient = exports.getClient = exports.getClients = void 0;
const client_model_1 = __importDefault(require("../models/client.model"));
const getClients = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const Clients = yield client_model_1.default.findAll({
        where: {
            estado: true
        }
    });
    res.json({
        msg: "Peticion exitosa",
        Clientes: Clients,
    });
});
exports.getClients = getClients;
const getClient = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const client = yield client_model_1.default.findByPk(id);
    if (client) {
        res.json({
            msg: "Peticion exitosa",
            Cliente: client,
        });
    }
    else {
        res.status(404).json({
            msg: `No existe un Client con el id ${id}`,
        });
    }
});
exports.getClient = getClient;
const postClient = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { body } = req;
    try {
        const existecorreo = yield client_model_1.default.findOne({
            where: {
                correo: body.correo,
            },
        });
        if (existecorreo) {
            return res.status(400).json({
                msg: "Ya existe un Client con el correo " + body.correo,
            });
        }
        const client = yield client_model_1.default.create(body);
        res.json(client);
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
});
exports.postClient = postClient;
const putClient = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const { body } = req;
    try {
        const client = yield client_model_1.default.findByPk(id);
        if (!client) {
            return res.status(404).json({
                msg: "No existe el Cliente con el " + id,
            });
        }
        const existecorreo = yield client_model_1.default.findOne({
            where: {
                correo: body.correo,
            },
        });
        if (existecorreo) {
            return res.status(400).json({
                msg: "Ya existe un Client con el correo " + body.correo,
            });
        }
        yield client.update(body);
        res.json(client);
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
});
exports.putClient = putClient;
const deleteClient = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const { body } = req;
    try {
        const client = yield client_model_1.default.findByPk(id);
        if (!client) {
            return res.status(404).json({
                msg: "No existe el Cliente con el " + id,
            });
        }
        yield client.update({ estado: false });
        res.json(client);
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
});
exports.deleteClient = deleteClient;
//# sourceMappingURL=clients.controller.js.map