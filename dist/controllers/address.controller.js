"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteAddress = exports.putAddress = exports.postAddress = exports.getAddressForClient = exports.getAddress = exports.getAddresses = void 0;
const address_model_1 = __importDefault(require("../models/address.model"));
const client_model_1 = __importDefault(require("../models/client.model"));
const getAddresses = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const address = yield address_model_1.default.findAll({
        where: {
            estado: true,
        },
        include: client_model_1.default,
    });
    res.json({
        msg: "Peticion exitosa",
        direccions: address,
    });
});
exports.getAddresses = getAddresses;
const getAddress = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const address = yield address_model_1.default.findByPk(id);
    if (address_model_1.default) {
        res.json({
            msg: "Peticion exitosa",
            address,
        });
    }
    else {
        res.status(404).json({
            msg: `No existe un Address con el id ${id}`,
        });
    }
});
exports.getAddress = getAddress;
const getAddressForClient = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const address = yield address_model_1.default.findAll({
        where: {
            clienteId: id,
            estado: true,
        },
    });
    if (address_model_1.default) {
        res.json({
            msg: "Peticion exitosa",
            address,
        });
    }
    else {
        res.status(404).json({
            msg: `No existe un Address con el id ${id}`,
        });
    }
});
exports.getAddressForClient = getAddressForClient;
const postAddress = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { body } = req;
    try {
        const address = yield address_model_1.default.create(body);
        res.json(address);
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
});
exports.postAddress = postAddress;
const putAddress = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const { body } = req;
    try {
        const address = yield address_model_1.default.findByPk(id);
        if (!address_model_1.default) {
            return res.status(404).json({
                msg: "No existe el direccion con el " + id,
            });
        }
        yield (address === null || address === void 0 ? void 0 : address.update(body));
        res.json(address);
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
});
exports.putAddress = putAddress;
const deleteAddress = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const { body } = req;
    try {
        const address = yield address_model_1.default.findByPk(id);
        if (!address_model_1.default) {
            return res.status(404).json({
                msg: "No existe el direccion con el " + id,
            });
        }
        yield (address === null || address === void 0 ? void 0 : address.update({ estado: false }));
        res.json(address);
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Hable con el administrador",
        });
    }
});
exports.deleteAddress = deleteAddress;
//# sourceMappingURL=address.controller.js.map