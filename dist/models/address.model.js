"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const client_model_1 = __importDefault(require("../models/client.model"));
const connection_1 = __importDefault(require("../db/connection"));
const Address = connection_1.default.define('direcciones', {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    ciudad: {
        type: sequelize_1.DataTypes.STRING
    },
    sector: {
        type: sequelize_1.DataTypes.STRING
    },
    calle: {
        type: sequelize_1.DataTypes.STRING
    },
    clienteId: {
        type: sequelize_1.DataTypes.INTEGER
    },
    estado: {
        type: sequelize_1.DataTypes.BOOLEAN
    },
});
Address.belongsTo(client_model_1.default);
exports.default = Address;
//# sourceMappingURL=address.model.js.map