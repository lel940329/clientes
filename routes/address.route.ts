import { Router } from 'express';
import { getAddresses,  getAddress, postAddress, putAddress, deleteAddress,getAddressForClient } from '../controllers/address.controller';


const router = Router(); 

router.get(   '/direcciones', getAddresses);
router.get(   '/direcciones/:id', getAddress);
router.get(   '/direcciones/clientes/:id', getAddressForClient);
router.post(  '/direcciones', postAddress);
router.put(   '/direcciones/:id', putAddress);
router.delete('/direcciones/:id', deleteAddress);


export default router;