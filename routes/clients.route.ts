import { Router } from 'express';
import { deleteClient, getClient, getClients, postClient, putClient } from '../controllers/clients.controller';

const router = Router(); 

router.get(   '/clientes', getClients);
router.get(   '/clientes/:id', getClient);
router.post(  '/clientes', postClient);
router.put(   '/clientes/:id', putClient);
router.delete('/clientes/:id', deleteClient);


export default router;