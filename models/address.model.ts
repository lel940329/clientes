import { DataTypes } from 'sequelize';
import Client from "../models/client.model";
import db from '../db/connection';

const Address = db.define('direcciones', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    ciudad: {
        type: DataTypes.STRING
    },
    sector: {
        type: DataTypes.STRING
    },
    calle: {
        type: DataTypes.STRING
    },
    clienteId: {
        type: DataTypes.INTEGER
    },
    estado: {
        type: DataTypes.BOOLEAN
    },
  
});

Address.belongsTo(Client);

export default Address;