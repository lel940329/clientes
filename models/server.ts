import express from "express";
import clientsRouter from "../routes/clients.route";
import addressRouter from "../routes/address.route";
import cors from "cors";
import db from "../db/connection";

class Server {
  private app: express.Application;
  private port: string;
  private apiPaths = {
    clients: "/api",
    adreess: "/api",
  };
  constructor() {
    this.app = express();
    this.port = process.env.PORT || "8000";
    this.dbconnetion();
    this.middleware();
    this.routes();
  }

  middleware() {
    this.app.use(cors());
    this.app.use(express.json());
    this.app.use(express.static("public"));
  }

  async dbconnetion() {
    try {
      await db.authenticate();
      console.log("Conectado exitosamente");
    } catch (error) {
      console.error('Unable to connect to the database:', error);
    }
  }

  routes() {
    this.app.use(this.apiPaths.clients, clientsRouter);
    this.app.use(this.apiPaths.adreess, addressRouter);
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log("Corriendo");
    });
  }
}

export default Server;
