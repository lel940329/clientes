import { Request, Response } from "express";
import Client from "../models/client.model";

export const getClients = async (req: Request, res: Response) => {
  const Clients = await Client.findAll({
    where:{
      estado: true
    }
  });

  res.json({
    msg: "Peticion exitosa",
    Clientes: Clients,
  });
};

export const getClient = async (req: Request, res: Response) => {
  const { id } = req.params;

  const client = await Client.findByPk(id);

  if (client) {
    res.json({
      msg: "Peticion exitosa",
      Cliente: client,
    });
  } else {
    res.status(404).json({
      msg: `No existe un Client con el id ${id}`,
    });
  }
};

export const postClient = async (req: Request, res: Response) => {
  const { body } = req;

  try {
    const existecorreo = await Client.findOne({
      where: {
        correo: body.correo,
      },
    });

    if (existecorreo) {
      return res.status(400).json({
        msg: "Ya existe un Client con el correo " + body.correo,
      });
    }
    const client = await Client.create(body);
    res.json(client);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

export const putClient = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { body } = req;
  try {
    const client = await Client.findByPk(id);

    if (!client) {
      return res.status(404).json({
        msg: "No existe el Cliente con el " + id,
      });
    }

    const existecorreo = await Client.findOne({
      where: {
        correo: body.correo,
      },
    });

    if (existecorreo) {
      return res.status(400).json({
        msg: "Ya existe un Client con el correo " + body.correo,
      });
    }

    await client.update(body);
    res.json(client);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

export const deleteClient = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { body } = req;
  try {
    const client = await Client.findByPk(id);

    if (!client) {
      return res.status(404).json({
        msg: "No existe el Cliente con el " + id,
      });
    }

    await client.update({ estado: false });
    res.json(client);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};
