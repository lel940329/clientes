import { Request, Response } from "express";
import Address from "../models/address.model";
import Client from "../models/client.model";

export const getAddresses = async (req: Request, res: Response) => {
  const address = await Address.findAll({
    where: {
      estado: true,
    },
    include: Client,
  });

  res.json({
    msg: "Peticion exitosa",
    direccions: address,
  });
};

export const getAddress = async (req: Request, res: Response) => {
  const { id } = req.params;

  const address = await Address.findByPk(id);

  if (Address) {
    res.json({
      msg: "Peticion exitosa",
      address,
    });
  } else {
    res.status(404).json({
      msg: `No existe un Address con el id ${id}`,
    });
  }
};

export const getAddressForClient = async (req: Request, res: Response) => {
  const { id } = req.params;

  const address = await Address.findAll({
    where: {
      clienteId: id,
      estado: true,
    },
  });

  if (Address) {
    res.json({
      msg: "Peticion exitosa",
      address,
    });
  } else {
    res.status(404).json({
      msg: `No existe un Address con el id ${id}`,
    });
  }
};

export const postAddress = async (req: Request, res: Response) => {
  const { body } = req;

  try {
    const address = await Address.create(body);
    res.json(address);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

export const putAddress = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { body } = req;
  try {
    const address = await Address.findByPk(id);

    if (!Address) {
      return res.status(404).json({
        msg: "No existe el direccion con el " + id,
      });
    }

    await address?.update(body);
    res.json(address);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

export const deleteAddress = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { body } = req;
  try {
    const address = await Address.findByPk(id);

    if (!Address) {
      return res.status(404).json({
        msg: "No existe el direccion con el " + id,
      });
    }

    await address?.update({ estado: false });
    res.json(address);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};
